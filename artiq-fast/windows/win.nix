{ pkgs ? import <nixpkgs> {}
, lib ? pkgs.lib
, diskImageSize ? "22G"
, qemuMem ? "4G"
, windowsImage ? null
, autoUnattendParams ? {}
, packages ? []
, impureMode ? false
, ...
}@attrs:

let
  # qemu_test is a smaller closure only building for a single system arch
  qemu = pkgs.qemu_test;
  libguestfs = pkgs.libguestfs-with-appliance.override {
    inherit qemu;
  };

  runQemuCommand = name: command: (
    pkgs.runCommandNoCC name { buildInputs = [ pkgs.p7zip qemu libguestfs ]; }
      (
        ''
          if ! test -f; then
            echo "KVM not available, bailing out" >> /dev/stderr
            exit 1
          fi
        '' + command
      )
  );

  windowsIso = if windowsImage != null then windowsImage else pkgs.fetchurl {
    url = "https://software-download.microsoft.com/download/sg/17763.107.101029-1455.rs5_release_svc_refresh_CLIENT_LTSC_EVAL_x64FRE_en-us.iso";
    sha256 = "668fe1af70c2f7416328aee3a0bb066b12dc6bbd2576f40f812b95741e18bc3a";
  };

  autounattend = import ./autounattend.nix (
    attrs // {
      inherit pkgs;
    }
  );

  bundleInstaller = pkgs.callPackage ./bundle {};

  # Packages required to drive installation of other packages
  bootstrapPkgs = let
    winPkgs = import ./pkgs.nix { inherit pkgs; };

    # Get updates from https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/
    virtioWin = winPkgs.makeMSIPkg {
      name = "virtio-win";
      cert = ./redhat-cert.cer;
      msi = pkgs.fetchurl {
        url = "https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/virtio-win-0.1.173-2/virtio-win-gt-x64.msi";
        sha256 = "0gmxw45fh22kxil1h2d42gxsri9diqfl7rdsxw2r261vvxrmrlq2";
      };
      ADDLOCAL = [
        "FE_balloon_driver"
        "FE_network_driver"
        "FE_pvpanic_driver"
        "FE_qemufwcfg_driver"
        "FE_qemupciserial_driver"
        "FE_qxl_driver"
        "FE_spice_driver"
        "FE_viorng_driver"
        "FE_vioscsi_driver"
        "FE_vioserial_driver"
        "FE_viostor_driver"
      ];
    };

    autohotkey = winPkgs.makePkg {
      name = "autohotkey";
      src = pkgs.fetchurl {
        url = "https://www.autohotkey.com/download/1.0/AutoHotkey104805_Install.exe";
        sha256 = "1f87w9g7f7dr0fq212vmg3zmabxw2013cf2i85zxdllyqbkw64a3";
      };
      installScript = ''
        .\AutoHotkey104805_Install.exe /S
      '';
    };

  in
    runQemuCommand "bootstrap-win-pkgs.img" ''
      mkdir pkgs
      mkdir pkgs/bootstrap
      mkdir pkgs/user

      cp ${autohotkey} pkgs/bootstrap/"$(stripHash "${autohotkey}")"
      cp ${bundleInstaller} pkgs/"$(stripHash "${bundleInstaller}")"

      ${lib.concatStringsSep "\n" (builtins.map (x: ''cp ${x} pkgs/bootstrap/"$(stripHash "${x}")"'') packages)}

      virt-make-fs --partition --type=fat pkgs/ $out
    '';

  installScript = pkgs.writeScript "windows-install-script" (
    let
      qemuParams = [
        "-enable-kvm"
        "-cpu"
        "host"
        "-smp"
        "$NIX_BUILD_CORES"
        "-m"
        "${qemuMem}"
        "-bios"
        "${pkgs.OVMF.fd}/FV/OVMF.fd"
        "-vga"
        "virtio"
        "-device"
        "piix3-usb-uhci" # USB root hub
        # USB boot
        "-drive"
        "id=win-install,file=usbimage.img,if=none,format=raw,readonly=on"
        "-device"
        "usb-storage,drive=win-install"
        # Output image
        "-drive"
        "file=c.img,index=0,media=disk,cache=unsafe"
        # "CD" drive with bootstrap pkgs
        "-drive"
        "id=virtio-win,file=${bootstrapPkgs},if=none,format=raw,readonly=on"
        "-device"
        "usb-storage,drive=virtio-win"
      ] ++ lib.optional (!impureMode) "-nographic";
    in
      ''
        #!${pkgs.runtimeShell}
        set -exuo pipefail
        export PATH=${lib.makeBinPath [ pkgs.p7zip qemu libguestfs ]}:$PATH

        # Create a bootable "USB" image
        # Booting in USB mode circumvents the "press any key to boot from cdrom" prompt
        #
        # Also embed the autounattend answer file in this image
        mkdir -p win
        mkdir -p win/nix-win
        7z x -y ${windowsIso} -owin
        cp ${autounattend} win/autounattend.xml
        virt-make-fs --partition --type=fat win/ usbimage.img
        rm -rf win

        # Qemu requires files to be rw
        qemu-img create -f qcow2 $out ${diskImageSize}
        env NIX_BUILD_CORES="''${NIX_BUILD_CORES:4}" qemu-system-x86_64 ${lib.concatStringsSep " " qemuParams}
      ''
  );

in
if impureMode then installScript else pkgs.runCommandNoCC "windows.img" {} ''
  ${installScript}
  mv c.img $out
''
