# Preparation steps

## Install a Windows image

1. Adjust build.nix accordingly
2. Run:

If in impure mode
```shell
nix-build build.nix
./result
```
Results in a file called c.img

If in pure mode
```shell
nix-build build.nix
ls -la ./result
```
Results in a symlink to the image in the nix store
